﻿using SuperZapatosGAP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using System.Threading.Tasks;

namespace SuperZapatosGAP.BLL
{
    public class bllArticles : IBllEntity<dtoArticles>
    {
        private readonly dcSuperZapatosGAP _db = new dcSuperZapatosGAP();       // TODO: Modificar para hacer loose coupling

        public bllArticles()
        {
            Mapper.CreateMap<articles, dtoArticles>();
            Mapper.CreateMap<dtoArticles, articles>();
        }

        public List<dtoArticles> GetAll()
        {
            try
            {
                var data = _db.articles;
                var result = new List<dtoArticles>();
                if (data.Any())
                {
                    foreach (var article in data)
                    {
                        var item = Mapper.Map<articles, dtoArticles>(article);
                        item.store_name = _db.stores.FirstOrDefault(p => p.id == article.store_id).name.ToString();
                        result.Add(item);
                    }
                }
                return result;
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        public dtoArticles GetById(int id)
        {
            try
            {
                var data = _db.articles.FirstOrDefault(p => p.id == id);
                dtoArticles result = null;
                if (data != null)
                {
                    result = Mapper.Map<articles, dtoArticles>(data);
                    result.store_name = _db.stores.FirstOrDefault(p => p.id == result.store_id).name.ToString();
                }
                return result;
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        public List<dtoArticles> GetByStoreId(int id)
        {
            try
            {
                var data = _db.articles.Where(p => p.store_id == id);
                var result = new List<dtoArticles>();
                if (data.Any())
                {
                    foreach (var article in data)
                    {
                        var item = Mapper.Map<articles, dtoArticles>(article);
                        result.Add(item);
                    }
                }
                return result;
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        public async Task<dtoArticles> Create(dtoArticles entity)
        {
            try
            {
                var item = Mapper.Map<dtoArticles, articles>(entity);
                _db.articles.Add(item);
                await _db.SaveChangesAsync();
                dtoArticles result = null;
                result = Mapper.Map<articles, dtoArticles>(item);
                result.store_name = _db.stores.FirstOrDefault(p => p.id == result.store_id).name.ToString();
                return result;
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        public async Task<dtoArticles> Update(dtoArticles entity)
        {
            try
            {
                var newItem = Mapper.Map<dtoArticles, articles>(entity);
                _db.articles.Attach(newItem);
                _db.Entry(newItem).State = System.Data.Entity.EntityState.Modified;
                await _db.SaveChangesAsync();
                dtoArticles result = null;
                result = Mapper.Map<articles, dtoArticles>(newItem);
                result.store_name = _db.stores.FirstOrDefault(p => p.id == result.store_id).name.ToString();
                return result;
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        public async Task<dtoArticles> Delete(dtoArticles entity)
        {
            try
            {
                var item = Mapper.Map<dtoArticles, articles>(entity);
                _db.Entry(item).State = System.Data.Entity.EntityState.Deleted;
                await _db.SaveChangesAsync();
                return entity;
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }
    }
}