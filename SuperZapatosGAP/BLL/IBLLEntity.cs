﻿using SuperZapatosGAP.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SuperZapatosGAP.BLL
{
    interface IBllEntity<T>
    {
        List<T> GetAll();
        T GetById(int id);
        Task<T> Create(T entity);
        Task<T> Update(T entity);
        Task<T> Delete(T entity);
    }
}
