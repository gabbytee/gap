﻿using SuperZapatosGAP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using System.Threading.Tasks;

namespace SuperZapatosGAP.BLL
{
    public class bllStores : IBllEntity<dtoStores>
    {
        private readonly dcSuperZapatosGAP _db = new dcSuperZapatosGAP();       // TODO: Modificar para hacer loose coupling

        public bllStores()
        {
            Mapper.CreateMap<stores, dtoStores>();
            Mapper.CreateMap<dtoStores, stores>();
        }

        public List<dtoStores> GetAll()
        {
            try
            {
                var data = _db.stores;
                var result = new List<dtoStores>();
                if (data.Any())
                {
                    foreach (var store in data)
                    {
                        var item = Mapper.Map<stores, dtoStores>(store);
                        result.Add(item);
                    }
                }
                return result;
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        public dtoStores GetById(int id)
        {
            try
            {
                var data = _db.stores.FirstOrDefault(p => p.id == id);
                dtoStores result = null;
                if (data != null)
                {
                    result = Mapper.Map<stores, dtoStores>(data);
                }
                return result;
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        public async Task<dtoStores> Create(dtoStores entity)
        {
            try
            {
                var item = Mapper.Map<dtoStores, stores>(entity);
                _db.stores.Add(item);
                await _db.SaveChangesAsync();
                return Mapper.Map<stores, dtoStores>(item);
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        public async Task<dtoStores> Update(dtoStores entity)
        {
            try
            {
                var newItem = Mapper.Map<dtoStores, stores>(entity);
                _db.stores.Attach(newItem);
                _db.Entry(newItem).State = System.Data.Entity.EntityState.Modified;
                await _db.SaveChangesAsync();
                return Mapper.Map<stores, dtoStores>(newItem);
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        public async Task<dtoStores> Delete(dtoStores entity)
        {
            try
            {
                var item = Mapper.Map<dtoStores, stores>(entity);
                _db.Entry(item).State = System.Data.Entity.EntityState.Deleted;
                await _db.SaveChangesAsync();
                return entity;
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }
    }
}