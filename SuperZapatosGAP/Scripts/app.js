﻿$(document).ready(function () {
    _.templateSettings = {
        interpolate: /\{\{\=(.+?)\}\}/g,
        evaluate: /\{\{(.+?)\}\}/g,
        escape: /\{\{\-(.+?)\}\}/g
    };

    models = {};

    entidades = {
        'Almacenes': 'Stores',
        'Artículos': 'Articles'
    };

    addEntidadHandler('Stores', 'Almacenes');
    addEntidadHandler('Articles', 'Artículos');
    addHeaderDataHandler();

    //CreateHandler
    $('#data').on('click', '.create-button', function () {
        var entity = $(this).attr('data-entity');
        //console.log('Create ' + entity);
        if (entity == 'Stores') {
            $('#modal-create-store').modal('show');
        }
        if (entity == 'Articles') {
            $.ajax({
                url: '../Stores/GetAll',
                type: 'GET',
                cache: false,
                headers: {
                    'Accept': 'application/json',
                },
                success: function (data) {
                    if (data.success && data.stores.length > 0) {
                        var stores = data.stores;
                        var txtOpts = '<option value="0">Seleccione el almacén</option>';
                        for (var i = 0; i < stores.length; i++) {
                            txtOpts += '<option value="' + stores[i].id + '">' + stores[i].name + '</option>';
                        }
                        $('#create-article-store').html(txtOpts);
                        $('#modal-create-article').modal('show');
                    }
                    else {
                        toastr.error('No pudieron recogerse los almacenes o no hay ninguno');
                    }
                },
                error: function (xhr) {
                    toastr.error('No pudieron recogerse los almacenes');
                }
            });
        }
    });

    $(document).on('click', '#save-create-store', function () {
        var name = $('#create-store-name').val();
        var address = $('#create-store-address').val();
        if (name != '' && address != '') {
            $.ajax({
                url: '../Stores/Create',
                type: 'POST',
                headers: {
                    'Accept': 'application/json'
                },
                data: {
                    name: name,
                    address: address
                },
                dataType: 'json',
                success: function (data) {
                    if (data.success) {
                        toastr.success('Operación exitosa');
                        $('.control-create-store').each(function (index) {
                            $(this).val('');
                        });
                        $('#modal-create-store').modal('hide');
                        $('#Stores').trigger('click');
                    }
                    else {
                        toastr.error(data.error_message);
                    }
                },
                error: function (xhr) {
                    toastr.error('Error al realizar la operación');
                }
            });
        }
        else {
            toastr.warning("Por favor complete todos los campos requeridos");
        }
    });

    $(document).on('click', '#save-create-article', function () {
        var name = $('#create-article-name').val();
        var description = $('#create-article-description').val();
        var price = parseInt($('#create-article-price').val());
        var total_in_shelf = parseInt($('#create-article-total_in_shelf').val());
        var total_in_vault = parseInt($('#create-article-total_in_vault').val());
        var store_id = parseInt($('#create-article-store option:selected').val());
        if (name != '' && price != '' && total_in_shelf != '' && total_in_vault != '' && store_id > 0) {
            $.ajax({
                url: '../Articles/Create',
                type: 'POST',
                headers: {
                    'Accept': 'application/json'
                },
                data: {
                    name: name,
                    description: description,
                    price: price,
                    total_in_shelf: total_in_shelf,
                    total_in_vault: total_in_vault,
                    store_id: store_id
                },
                dataType: 'json',
                success: function (data) {
                    if (data.success) {
                        toastr.success('Operación exitosa');
                        $('.control-create-article').each(function (index) {
                            $(this).val('');
                        });
                        $('#modal-create-article').modal('hide');
                        $('#Articles').trigger('click');
                    }
                    else {
                        toastr.error(data.error_message);
                    }
                },
                error: function (xhr) {
                    toastr.error('Error al realizar la operación');
                }
            });
        }
        else {
            toastr.warning("Por favor complete todos los campos requeridos");
        }
    });

    //UpdateHandler
    $('#data').on('click', '.edit-button', function () {
        var entity = $(this).attr('data-entity');
        var id = $(this).attr('data-id');
        //console.log('Update ' + $(this).attr('data-entity') + ' id:' + id);
        if (entity == 'Stores') {
            var store = _.find(models.model, function (item) {
                return item.id == id;
            });
            $('#update-store-id').val(parseInt(id));
            $('#update-store-name').val(store.name);
            $('#update-store-address').val(store.address);
            $('#modal-update-store').modal('show');
        }
        if (entity == 'Articles') {
            $.ajax({
                url: '../Stores/GetAll',
                type: 'GET',
                cache: false,
                headers: {
                    'Accept': 'application/json',
                },
                success: function (data) {
                    if (data.success && data.stores.length > 0) {
                        var stores = data.stores;
                        var txtOpts = '<option value="0">Seleccione el almacén</option>';
                        for (var i = 0; i < stores.length; i++) {
                            txtOpts += '<option value="' + stores[i].id + '">' + stores[i].name + '</option>';
                        }
                        $('#update-article-store').html(txtOpts);
                        var article = _.find(models.model, function (item) {
                            return item.id == id;
                        });
                        $('#update-article-id').val(parseInt(id));
                        $('#update-article-name').val(article.name);
                        $('#update-article-description').val(article.description);
                        $('#update-article-price').val(article.price);
                        $('#update-article-total_in_shelf').val(article.total_in_shelf);
                        $('#update-article-total_in_vault').val(article.total_in_vault);
                        $('#update-article-store option[value=' + article.store_id + ']').prop('selected', 'selected');
                        $('#modal-update-article').modal('show');
                    }
                    else {
                        toastr.error('No pudieron recogerse los almacenes o no hay ninguno');
                    }
                },
                error: function (xhr) {
                    toastr.error('No pudieron recogerse los almacenes');
                }
            });
        }

    });

    $(document).on('click', '#save-update-store', function () {
        var id = $('#update-store-id').val();
        var name = $('#update-store-name').val();
        var address = $('#update-store-address').val();
        if (name != '' && address != '') {
            $.ajax({
                url: '../Stores/Update',
                type: 'POST',
                headers: {
                    'Accept': 'application/json'
                },
                data: {
                    id: id,
                    name: name,
                    address: address
                },
                dataType: 'json',
                success: function (data) {
                    if (data.success) {
                        toastr.success('Operación exitosa');
                        $('.control-update-store').each(function (index) {
                            $(this).val('');
                        });
                        $('#modal-update-store').modal('hide');
                        $('#Stores').trigger('click');
                    }
                    else {
                        toastr.error(data.error_message);
                    }
                },
                error: function (xhr) {
                    toastr.error('Error al realizar la operación');
                }
            });
        }
        else {
            toastr.warning("Por favor complete todos los campos requeridos");
        }
    });

    $(document).on('click', '#save-update-article', function () {
        var id = $('#update-article-id').val();
        var name = $('#update-article-name').val();
        var description = $('#update-article-description').val();
        var price = parseInt($('#update-article-price').val());
        var total_in_shelf = parseInt($('#update-article-total_in_shelf').val());
        var total_in_vault = parseInt($('#update-article-total_in_vault').val());
        var store_id = parseInt($('#update-article-store option:selected').val());
        if (name != '' && price != '' && total_in_shelf != '' && total_in_vault != '' && store_id > 0) {
            $.ajax({
                url: '../Articles/Update',
                type: 'POST',
                headers: {
                    'Accept': 'application/json'
                },
                data: {
                    id: id,
                    name: name,
                    description: description,
                    price: price,
                    total_in_shelf: total_in_shelf,
                    total_in_vault: total_in_vault,
                    store_id: store_id
                },
                dataType: 'json',
                success: function (data) {
                    if (data.success) {
                        toastr.success('Operación exitosa');
                        $('.control-update-article').each(function (index) {
                            $(this).val('');
                        });
                        $('#modal-update-article').modal('hide');
                        $('#Articles').trigger('click');
                    }
                    else {
                        toastr.error(data.error_message);
                    }
                },
                error: function (xhr) {
                    toastr.error('Error al realizar la operación');
                }
            });
        }
        else {
            toastr.warning("Por favor complete todos los campos requeridos");
        }
    });

    //DeleteHandler
    $('#data').on('click', '.delete-button', function () {
        var entity = $(this).attr('data-entity');
        var id = $(this).attr('data-id');
        //console.log('Update ' + $(this).attr('data-entity') + ' id:' + id);
        if (entity == 'Stores') {
            var store = _.find(models.model, function (item) {
                return item.id == id;
            });
            $('#delete-store-id').val(parseInt(id));
            $('#delete-store-name').val(store.name);
            $('#delete-store-address').val(store.address);
            $('#modal-delete-store').modal('show');
        }
        if (entity == 'Articles') {
            var article = _.find(models.model, function (item) {
                return item.id == id;
            });
            $('#delete-article-id').val(parseInt(id));
            $('#delete-article-name').val(article.name);
            $('#delete-article-description').val(article.description);
            $('#delete-article-price').val(article.price);
            $('#delete-article-price-total_in_shelf').val(article.total_in_shelf);
            $('#delete-article-price-total_in_vault').val(article.total_in_vault);
            $('#delete-article-store_id').val(article.store_id);
            $('#modal-delete-article').modal('show');
        }
    });

    $(document).on('click', '#save-delete-store', function () {
        var id = $('#delete-store-id').val();
        var name = $('#delete-store-name').val();
        var address = $('#delete-store-address').val();
        $.ajax({
            url: '../Stores/Delete',
            type: 'POST',
            headers: {
                'Accept': 'application/json'
            },
            data: {
                id: id,
                name: name,
                address: address
            },
            dataType: 'json',
            success: function (data) {
                if (data.success) {
                    toastr.success('Operación exitosa');
                    $('#modal-delete-store').modal('hide');
                    $('#Stores').trigger('click');
                }
                else {
                    toastr.error(data.error_message);
                }
            },
            error: function (xhr) {
                toastr.error('Error al realizar la operación');
            }
        });
    });

    $(document).on('click', '#save-delete-article', function () {
        var id = $('#delete-article-id').val();
        var name = $('#delete-article-name').val();
        var description = $('#delete-article-description').val();
        var price = $('#delete-article-price').val();
        var total_in_shelf = $('#delete-article-price-total_in_shelf').val();
        var total_in_vault = $('#delete-article-price-total_in_vault').val();
        var store_id = $('#delete-article-store_id').val();
        $.ajax({
            url: '../Articles/Delete',
            type: 'POST',
            headers: {
                'Accept': 'application/json'
            },
            data: {
                id: id,
                name: name,
                description: description,
                price: price,
                total_in_shelf: total_in_shelf,
                total_in_vault: total_in_vault,
                store_id: store_id
            },
            dataType: 'json',
            success: function (data) {
                if (data.success) {
                    toastr.success('Operación exitosa');
                    $('#modal-delete-article').modal('hide');
                    $('#Articles').trigger('click');
                }
                else {
                    toastr.error(data.error_message);
                }
            },
            error: function (xhr) {
                toastr.error('Error al realizar la operación');
            }
        });
    });


});

function View(model, elemTemplate, args) {
    this.model = model;
    this.template = _.template($(elemTemplate).html());
    this.args = args;
    this.render = function (el) {
        $(el).html(this.template(this.model));
    };
};

function addEntidadHandler(entidad, entidadName) {
    $('#' + entidad).click(function () {
        $('#titulo_entidad').html(entidadName);
        renderLoading('#data');
        $.ajax({
            url: '../' + entidad + '/GetAll',
            type: 'GET',
            cache: false,
            headers: {
                'Accept': 'application/json',
            },
            success: function (response) {
                renderTable(response[entidad.toLowerCase()], entidad);
            },
            error: function (xhr) {
                renderTable(xhr.responseText, '');
            }
        });
    });

}

function renderTable(data, entidad) {
    if (data.length > 0 && typeof data == 'object') {
        var count = 0;
        _.each(data, function (value, key, list) {
            count++;
            if (entidad == 'Articles') {
                list[key].strPrice = list[key].price.formatMoney(2, ',', '.', '$');
            }
        });
        models.model = data;
        var view = new View(
            data,
            '#tpl_data_table_' + entidad.toLowerCase(),
            {
                entidad: entidad,
                count: count
            }
        );
        view.render('#data');
    }
    else if (data.length == 0 && typeof data == 'object') {
        $('#data').html('<p class="disabled">Sin datos</p>');
    }
    else {
        $('#data').html(data);
    }
}

function addHeaderDataHandler() {
    $('#data').on('click', '.sortable', function (event) {
        var elem = $(this);
        var elemSelector = fullPath(elem[0]);
        var fieldName = elem.attr('data-field');
        var fieldType = elem.attr('data-type');
        var newModel = models.model;
        var entidad = entidades[$('#titulo_entidad').html()];
        if ($(elemSelector).hasClass('sorted')) {
            if ($(elemSelector).hasClass('sorted-descending')) {
                newModel = _.sortBy(models.model, (fieldType == 'string' ? Comparator.string_comparator(fieldName) : fieldName));
                renderTable(newModel, entidad);
            } else {
                newModel = _.sortBy(models.model, (fieldType == 'string' ? Comparator.string_comparator(fieldName) : fieldName)).reverse();
                renderTable(newModel, entidad);
                $(elemSelector).addClass('sorted-descending');
            }
        } else {
            newModel = _.sortBy(models.model, (fieldType == 'string' ? Comparator.string_comparator(fieldName) : fieldName));
            renderTable(newModel, entidad);
        }
        $(elemSelector).addClass('sorted');
    });
}

Comparator = {
    string_comparator: function (param_name, compare_depth) {
        if (param_name[0] == '-') {
            param_name = param_name.slice(1),
            compare_depth = compare_depth || 10;
            return function (item) {
                return String.fromCharCode.apply(String,
                   _.map(item[param_name].slice(0, compare_depth).split(""), function (c) {
                       return 0xffff - c.charCodeAt();
                   })
               );
            };
        } else {
            return function (item) {
                return item[param_name];
            };
        }
    },
    number_comparator: function (param_name) {
        if (param_name[0] == '-') {
            return function (item) {
                return item * -1;
            };
        } else {
            return function (item) {
                return item;
            };
        }
    }
}

function renderLoading(el) {
    var img = '../Imgs/ajax-loader.gif';
    $(el).html("<img src='" + img + "' border='0' />")
}

function fullPath(el) {
    var names = [];
    while (el.parentNode) {
        if (el.id) {
            names.unshift('#' + el.id);
            break;
        } else {
            if (el == el.ownerDocument.documentElement) names.unshift(el.tagName);
            else {
                for (var c = 1, e = el; e.previousElementSibling; e = e.previousElementSibling, c++);
                names.unshift(el.tagName + ":nth-child(" + c + ")");
            }
            el = el.parentNode;
        }
    }
    return names.join(" > ");
}

Number.prototype.formatMoney = function (decPlaces, thouSeparator, decSeparator, currencySymbol) {
    decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces;
    decSeparator = decSeparator == undefined ? "." : decSeparator;
    thouSeparator = thouSeparator == undefined ? "," : thouSeparator;
    currencySymbol = currencySymbol == undefined ? "$" : currencySymbol;
    var n = this,
        sign = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return sign + currencySymbol + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
}
