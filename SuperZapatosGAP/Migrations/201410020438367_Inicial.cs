namespace SuperZapatosGAP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Inicial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.articles",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false, maxLength: 200),
                        description = c.String(nullable: false, unicode: false, storeType: "text"),
                        price = c.Decimal(nullable: false, precision: 7, scale: 2),
                        total_in_shelf = c.Int(nullable: false),
                        total_in_vault = c.Int(nullable: false),
                        store_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.stores", t => t.store_id)
                .Index(t => t.store_id);
            
            CreateTable(
                "dbo.stores",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false, maxLength: 200),
                        address = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.articles", "store_id", "dbo.stores");
            DropIndex("dbo.articles", new[] { "store_id" });
            DropTable("dbo.stores");
            DropTable("dbo.articles");
        }
    }
}
