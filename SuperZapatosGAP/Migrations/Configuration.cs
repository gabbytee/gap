namespace SuperZapatosGAP.Migrations
{
    using SuperZapatosGAP.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<SuperZapatosGAP.Models.dcSuperZapatosGAP>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(SuperZapatosGAP.Models.dcSuperZapatosGAP context)
        {
            var stores = new List<stores>
            {
                new stores {id = 1, name="Alpha Store", address="1001 Alpha Street"},
                new stores {id = 2, name="Beta Store", address="2002 Beta Street"},
                new stores {id = 3, name="Gamma Store", address="3003 Gamma Street"},
            };
            stores.ForEach(s => context.stores.AddOrUpdate(p => p.id, s));
            context.SaveChanges();

            var articles = new List<articles> {
                new articles {id = 1, name="First Alpha Product", description="First Alpha Product", price=1.10m, total_in_shelf=10, total_in_vault=100, store_id=1},
                new articles {id = 2, name="Second Alpha Product", description="Second Alpha Product", price=2.20m, total_in_shelf=20, total_in_vault=200, store_id=1},
                new articles {id = 3, name="Third Alpha Product", description="Third Alpha Product", price=3.30m, total_in_shelf=30, total_in_vault=300, store_id=1},
                new articles {id = 4, name="First Beta Product", description="First Beta Product", price=1.10m, total_in_shelf=10, total_in_vault=100, store_id=2},
                new articles {id = 5, name="Second Beta Product", description="Second Beta Product", price=2.20m, total_in_shelf=20, total_in_vault=200, store_id=2},
                new articles {id = 6, name="Third Beta Product", description="Third Beta Product", price=3.30m, total_in_shelf=30, total_in_vault=300, store_id=2},
                new articles {id = 7, name="First Gamma Product", description="First Gamma Product", price=1.10m, total_in_shelf=10, total_in_vault=100, store_id=3},
                new articles {id = 8, name="Second Gamma Product", description="Second Gamma Product", price=2.20m, total_in_shelf=20, total_in_vault=200, store_id=3},
                new articles {id = 9, name="Third Gamma Product", description="Third Gamma Product", price=3.30m, total_in_shelf=30, total_in_vault=300, store_id=3},
            };
            articles.ForEach(a => context.articles.AddOrUpdate(q => q.id, a));
            context.SaveChanges();
        }
    }
}
