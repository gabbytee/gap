﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace SuperZapatosGAP.Models
{
    public class dtoStoresResponse
    {
        public bool success { get; set; }
        [XmlArrayItem("store")]
        public List<dtoStores> stores { get; set; }
        public int total_elements { get; set; }
    }
}