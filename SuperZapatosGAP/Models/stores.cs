namespace SuperZapatosGAP.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class stores
    {
        public stores()
        {
            articles = new HashSet<articles>();
        }

        [Key]
        public int id { get; set; }

        [Required]
        [StringLength(200)]
        public string name { get; set; }

        [Required]
        [StringLength(100)]
        public string address { get; set; }

        public virtual ICollection<articles> articles { get; set; }
    }
}
