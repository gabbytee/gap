namespace SuperZapatosGAP.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class articles
    {
        [Key]
        public int id { get; set; }

        [Required]
        [StringLength(200)]
        public string name { get; set; }

        [Column(TypeName = "text")]
        [Required]
        public string description { get; set; }

        public decimal price { get; set; }

        public int total_in_shelf { get; set; }

        public int total_in_vault { get; set; }

        public int store_id { get; set; }

        [NotMapped]
        public string store_name { get; set; }

        public virtual stores stores { get; set; }
    }
}
