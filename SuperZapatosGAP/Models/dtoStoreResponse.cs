﻿namespace SuperZapatosGAP.Models
{
    public class dtoStoreResponse
    {
        public bool success { get; set; }
        public dtoStores store { get; set; }
        public int total_elements { get; set; }
    }
}