﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace SuperZapatosGAP.Models
{
    public class dtoArticlesResponse
    {
        public bool success { get; set; }
        [XmlArrayItem("article")]
        public List<dtoArticles> articles { get; set; }
        public int total_elements { get; set; }
    }
}