﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SuperZapatosGAP.Models
{
    public class dtoStores
    {
        public int id { get; set; }
        public string name { get; set; }
        public string address { get; set; }
    }
}