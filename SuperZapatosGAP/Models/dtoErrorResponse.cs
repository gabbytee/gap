﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SuperZapatosGAP.Models
{
    public class dtoErrorResponse
    {
        public bool success { get; set; }
        public int error_code { get; set; }
        public string error_message { get; set; }
    }
}