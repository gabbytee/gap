namespace SuperZapatosGAP.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class dcSuperZapatosGAP : DbContext
    {
        public dcSuperZapatosGAP()
            : base("name=dcSuperZapatosGAP")
        {
            this.Configuration.LazyLoadingEnabled = false;
        }

        public virtual DbSet<articles> articles { get; set; }
        public virtual DbSet<stores> stores { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<articles>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<articles>()
                .Property(e => e.price)
                .HasPrecision(7, 2);

            modelBuilder.Entity<stores>()
                .HasMany(e => e.articles)
                .WithRequired(e => e.stores)
                .HasForeignKey(e => e.store_id)
                .WillCascadeOnDelete(false);
        }
    }
}
