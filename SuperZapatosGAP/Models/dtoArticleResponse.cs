﻿namespace SuperZapatosGAP.Models
{
    public class dtoArticleResponse
    {
        public bool success { get; set; }
        public dtoArticles article { get; set; }
        public int total_elements { get; set; }
    }
}