﻿using SuperZapatosGAP.BLL;
using SuperZapatosGAP.Models;
using SuperZapatosGAP.Utils;
using System;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Mvc;

namespace SuperZapatosGAP.Controllers
{
    public class ServicesController : Controller
    {
        private readonly bllArticles _bllArticles = new bllArticles();          // TODO: Implementar IoC del controlador
        private readonly bllStores _bllStores = new bllStores();                // TODO: Implementar IoC del controlador
        private object _result = null;

        private bool validateBasicAuth()
        {
            var validUser = false;
            try
            {
                var authHeader = HttpContext.Request.Headers["Authorization"];
                if (authHeader != null)
                {
                    var authHeaderVal = AuthenticationHeaderValue.Parse(authHeader);
                    if (authHeaderVal.Scheme.Equals("basic", StringComparison.OrdinalIgnoreCase) && authHeaderVal.Parameter != null)
                    {
                        var encoding = Encoding.GetEncoding("iso-8859-1");
                        var credentials = encoding.GetString(Convert.FromBase64String(authHeaderVal.Parameter));
                        int separator = credentials.IndexOf(':');
                        string name = credentials.Substring(0, separator);
                        string password = credentials.Substring(separator + 1);
                        validUser = (name == "my_user" && password == "my_password");
                    }
                }
            }
            catch (Exception)
            {
                validUser = false;
            }
            return validUser;
        }

        private ActionResult responseByHeaders(object obj)
        {
            try
            {
                var resp = new object();
                var acceptHeader = ControllerContext.HttpContext.Request.Headers["Accept"];
                if (acceptHeader == "text/json" || acceptHeader == "application/json")
                {

                    resp = Json(obj, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    resp = new XmlResult(_result);
                }
                return (ActionResult)resp;
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        [HttpGet]
        public ActionResult GetArticlesByStoreId(string id)
        {
            try
            {
                if (validateBasicAuth())
                {
                    var Id = Convert.ToInt32(id);
                    var data = _bllArticles.GetByStoreId(Id);
                    if (data != null && data.Count > 0)
                    {
                        _result = new dtoArticlesResponse { articles = data, success = true, total_elements = data.Count };
                    }
                    else
                    {
                        _result = new dtoErrorResponse { success = false, error_code = 404, error_message = "Record not found" };
                    }
                }
                else
                {
                    _result = new dtoErrorResponse { success = false, error_code = 401, error_message = "Not authorized" };
                }
            }
            catch (FormatException)
            {
                _result = new dtoErrorResponse { success = false, error_code = 400, error_message = "Bad request" };
            }
            catch (Exception exc)
            {
                _result = new dtoErrorResponse { success = false, error_code = 500, error_message = exc.Message };
            }
            return responseByHeaders(_result);
        }

        [HttpGet]
        public ActionResult articles()
        {
            try
            {
                if (validateBasicAuth())
                {
                    var data = _bllArticles.GetAll();
                    if (data != null && data.Count > 0)
                    {
                        _result = new dtoArticlesResponse { articles = data, success = true, total_elements = data.Count };
                    }
                    else
                    {
                        _result = new dtoErrorResponse { success = false, error_code = 404, error_message = "Record not found" };
                    }
                }
                else
                {
                    _result = new dtoErrorResponse { success = false, error_code = 401, error_message = "Not authorized" };
                }
            }
            catch (Exception)
            {
                _result = new dtoErrorResponse { success = false, error_code = 500, error_message = "Server error" };
            }
            return responseByHeaders(_result);
        }

        [HttpGet]
        public ActionResult stores()
        {
            try
            {
                if (validateBasicAuth())
                {
                    var data = _bllStores.GetAll();
                    if (data != null && data.Count > 0)
                    {
                        _result = new dtoStoresResponse { stores = data, success = true, total_elements = data.Count };
                    }
                    else
                    {
                        _result = new dtoErrorResponse { success = false, error_code = 404, error_message = "Record not found" };
                    }
                }
                else
                {
                    _result = new dtoErrorResponse { success = false, error_code = 401, error_message = "Not authorized" };
                }
            }
            catch (Exception)
            {
                _result = new dtoErrorResponse { success = false, error_code = 500, error_message = "Server error" };
            }
            return responseByHeaders(_result);
        }
    }
}
