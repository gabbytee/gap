﻿using SuperZapatosGAP.BLL;
using SuperZapatosGAP.Models;
using SuperZapatosGAP.Utils;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SuperZapatosGAP.Controllers
{
    public class ArticlesController : Controller
    {
        private readonly bllArticles _bll = new bllArticles();          // TODO: Implementar IoC del controlador
        private object _result = null;

        private ActionResult responseByHeaders(object obj)
        {
            try
            {
                var resp = new object();
                var acceptHeader = ControllerContext.HttpContext.Request.Headers["Accept"];
                if (acceptHeader == "text/json" || acceptHeader == "application/json")
                {

                    resp = Json(obj, JsonRequestBehavior.AllowGet); 
                }
                else
                {
                    resp = new XmlResult(_result);
                }
                return (ActionResult) resp;
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        [HttpGet]
        public ActionResult GetAll()
        {
            try
            {
                var data = _bll.GetAll();
                if (data != null)
                {
                    _result = new dtoArticlesResponse { articles = data, success = true, total_elements = data.Count };
                }
                else
                {
                    _result = new dtoErrorResponse { success = false, error_code = 404, error_message = "Record not found" };
                }
            }
            catch (Exception)
            {
                _result = new dtoErrorResponse { success = false, error_code = 500, error_message = "Server error" };
            }
            return responseByHeaders(_result);
        }

        [HttpGet]
        public ActionResult GetById(int id)
        {
            try
            {
                var data = _bll.GetById(id);
                if (data != null)
                {
                    _result = new dtoArticleResponse { article = data, success = true, total_elements = (data == null ? 0 : 1) };
                }
                else
                {
                    _result = new dtoErrorResponse { success = false, error_code = 404, error_message = "Record not found" };
                }
            }
            catch (Exception exc)
            {
                _result = new dtoErrorResponse { success = false, error_code = 500, error_message = exc.Message };
            }
            return responseByHeaders(_result);
        }

        [HttpPost]
        public async Task<ActionResult> Create(dtoArticles entity)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var data = await _bll.Create(entity);
                    _result = new dtoArticleResponse { article = data, success = true, total_elements = (data == null ? 0 : 1) };
                }
                else
                {
                    _result = new dtoErrorResponse { success = false, error_code = 400, error_message = "Invalid model" };
                }

            }
            catch (Exception exc)
            {
                _result = new dtoErrorResponse { success = false, error_code = 500, error_message = exc.Message };
            }
            return responseByHeaders(_result);
        }

        [HttpPost]
        public async Task<ActionResult> Update(dtoArticles entity)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var data = await _bll.Update(entity);
                    _result = new dtoArticleResponse { article = data, success = true, total_elements = (data == null ? 0 : 1) };
                }
                else
                {
                    _result = new dtoErrorResponse { success = false, error_code = 400, error_message = "Invalid model" };
                }
            }
            catch (Exception exc)
            {
                _result = new dtoErrorResponse { success = false, error_code = 500, error_message = exc.Message };
            }
            return responseByHeaders(_result);
        }

        [HttpPost]
        public async Task<ActionResult> Delete(dtoArticles entity)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var data = await _bll.Delete(entity);
                    _result = new dtoArticleResponse { article = data, success = true, total_elements = (data == null ? 0 : 1) };
                }
                else
                {
                    _result = new dtoErrorResponse { success = false, error_code = 400, error_message = "Invalid model" };
                }
            }
            catch (Exception exc)
            {
                _result = new dtoErrorResponse { success = false, error_code = 500, error_message = exc.Message };
            }
            return responseByHeaders(_result);
        }
    }
}